# frozen_string_literal: true

require 'net/telnet'
require 'rspec'

# # https://github.com/cucumber/cucumber/wiki/Step-Argument-Transforms
# Transform /^(-?\d+)$/ do |number|
#   number.to_i
# end

# # These definitions hook into steps like this: 'I should see "{number}" items'
# ParameterType(
#   name: 'number',
#   # Note: the RegEx no longer includes the ^ and $ delimiter:
#   regexp: /(-?\d+)/,
#   type: Number,
#   transformer: lambda do |number_string|
#     number_string.to_i
#   end
# )

Given(/^a new random user$/) do
  @user = {}
  @user[:acct_name] = random_acct_name
  @user[:acct_email] = "#{@user[:acct_name]}@301days.com"
  @user[:acct_password] = random_acct_password
  @user[:char_name] = random_char_name
  @user[:char_gender] = %w[1 2].sample
  @user[:char_race] = %w[I M L LG D H MN B].sample
  @user[:char_class] = %w[FI TH WI MA TF].sample
  debug_msg "New random user: #{@user.inspect}"
end

Given(/^a silly account name$/) do
  @user[:acct_name] = random_acct_name(5, 12, 'testpvp')
end

Given(/^a profane account name$/) do
  @user[:acct_name] = random_acct_name(5, 12, 'testanalyst')
end

Given(/^a telnet connection$/) do
  @connection = create_telnet_connection
end

When(/^I create an account and character$/) do
  telnet_command(@connection, "\n\n\n", /Login: /, { 'Timeout' => 120 })
  telnet_commands(@connection, [['new', /account: /],
                                [@user[:acct_name], /address: /],
                                [@user[:acct_email], /address: /],
                                [@user[:acct_email], /12\): /],
                                [@user[:acct_password], /password: /],
                                [@user[:acct_password], /Gender: /],
                                [@user[:char_gender], /a Race: /],
                                [@user[:char_race], /Class: /],
                                [@user[:char_class], /\(y,n\): /],
                                ['n', /character: /],
                                [@user[:char_name], /Command: /]])
end

When(/^I try to create an account and the account name is rejected$/) do
  telnet_command(@connection, '', /Login: /, { 'Timeout' => 120 })
  telnet_command(@connection, 'new', /account: /)
  resp = telnet_command(@connection, @user[:acct_name], /.*:.*/)
  expect(resp).to include('That name is invalid.')
  expect(resp).not_to include('enter your email address:')
end

When(/^I log out$/) do
  @connection.write("3\n")
  sleep 1
end

When(/^I log in$/) do
  @connection = create_telnet_connection
  telnet_command(@connection, '', /Login: /, { 'Timeout' => 120 })
  telnet_commands(@connection, [[@user[:acct_name], /Password: /],
                                [@user[:acct_password], /Command: /]])
end

When(/^I log in with the wrong then right password$/) do
  @connection = create_telnet_connection
  telnet_command(@connection, '', /Login: /, { 'Timeout' => 120 })
  resp = telnet_commands(@connection, [[@user[:acct_name], /Password: /],
                                       ['oops', /:/]])
  expect(resp).to include('Invalid password.')
  telnet_commands(@connection, [[@user[:acct_name], /Password: /],
                                [@user[:acct_password], /Command: /]])
end

Then(/^I can play the game$/) do
  resp = telnet_command(@connection, '1', / ->/)
  expect(resp).to include('Experience :')
end

When(/^I enter the chat$/) do
  resp = telnet_command(@connection, '2', /list of commands./)
  expect(resp).to include('You are in Conference Room')
end

When(/^I exit the chat$/) do
  telnet_command(@connection, '/quit', /Command:/)
end

When(/^I exit the chat and am immediately disconnected$/) do
  expect { telnet_command_slow(@connection, '/quit', /_unlikely_text_/) }.to raise_error(EOFError)
end

When(/^I issue the shutdown command and am immediately disconnected$/) do
  # Depending on the environment, we might get EOFError or Errno::ECONNRESET

  telnet_command_slow(@connection, '/shutdown', /_unlikely_text_/)
rescue StandardError => e
  expect(e).to be_kind_of(EOFError).or be_kind_of(Errno::ECONNRESET)
end

Then(/^I get disconnected$/) do
  expect do
    @connection.waitfor({ 'String' => 'disconnected', 'FailEOF' => true })
  end.to raise_error(EOFError)
end

When(/^I create an account to verify that race "([^"]*)" has the following stat constraints:$/) do
  |race, table|
  start_time = Time.now
  mins = {}
  maxes = {}
  table.hashes.each do |row|
    mins[row[:stat]] = row[:min].to_i
    maxes[row[:stat]] = row[:max].to_i
  end

  @user[:char_race] = race
  telnet_command(@connection, "\n\n\n", /Login: /, { 'Timeout' => 120 })
  stats_string = telnet_commands(@connection, [['new', /account: /],
                                               [@user[:acct_name], /address: /],
                                               [@user[:acct_email], /address: /],
                                               [@user[:acct_email], /12\): /],
                                               [@user[:acct_password], /password: /],
                                               [@user[:acct_password], /Gender: /],
                                               [@user[:char_gender], /a Race: /],
                                               [@user[:char_race], /Class: /],
                                               [@user[:char_class], /\(y,n\): /]])
  rolls = 1
  mins_needed = mins.keys
  maxes_needed = maxes.keys
  until mins_needed.empty? && maxes_needed.empty?
    stats_string = telnet_command_fast(@connection, 'y', /\(y,n\): /) unless rolls == 1
    stats = parse_stats(stats_string)
    mins.each_key do |stat|
      expect(stats[stat]).to be >= mins[stat]
      # Doesn't work since the die rolls are being inflated:
      # expect(stats[stat]).to be <= maxes[stat]
      expect(stats[stat]).to be <= 18
    end
    stats.each_key do |stat|
      mins_needed.delete(stat) if stats[stat] == mins[stat]
      maxes_needed.delete(stat) if stats[stat] == maxes[stat]
    end
    debug_msg "Still looking for mins_needed #{mins_needed} and maxes_needed #{maxes_needed}."
    rolls += 1
    debug_msg "Rolls so far: #{rolls} Time so far: #{Time.now - start_time}"
    fail('Took too long to see min/max!') if rolls > 2000
  end
  debug_msg "Rolls taken to achieve stats: #{rolls}"
  telnet_commands(@connection, [['n', /character: /],
                                [@user[:char_name], /Command: /]])
end

Then(/^I can connect to the server via telnet$/) do
  expect(create_telnet_connection).to be_truthy
end

Then(/^I can not connect to the server via telnet$/) do
  if (temp_connection = create_telnet_connection)
    expect { telnet_command(temp_connection, "\n", /:/) }.to raise_error(EOFError)
  end
end

When(/^I disconnect from the server$/) do
  @connection.close
end

When(/^I log on using a developer account$/) do
  @user = { acct_name: 'dev01', acct_password: 'devpass01' }
  ensure_developer_account_exists(@user[:acct_name], @user[:acct_password])
  @connection = create_telnet_connection
  telnet_command(@connection, '', /Login: /, { 'Timeout' => 120 })
  telnet_commands(@connection, [[@user[:acct_name], /Password: /],
                                [@user[:acct_password], /Command: /]])
end

When(/^I log on using a standard account$/) do
  @user = { acct_name: 'user01', acct_password: 'userpass01' }
  ensure_standard_account_exists(@user[:acct_name], @user[:acct_password])
  @connection = create_telnet_connection
  telnet_command(@connection, '', /Login: /, { 'Timeout' => 120 })
  telnet_commands(@connection, [[@user[:acct_name], /Password: /],
                                [@user[:acct_password], /Command: /]])
end

When(/^I log on as "([^"]*)"$/) do |name|
  @user = { acct_name: name, acct_password: 'password' }
  ensure_standard_account_exists(@user[:acct_name], @user[:acct_password])
  @connection = create_telnet_connection
  telnet_command(@connection, '', /Login: /, { 'Timeout' => 120 })
  telnet_commands(@connection, [[@user[:acct_name], /Password: /],
                                [@user[:acct_password], /Command: /]])
end

When(/^I cause "([^"]*)" to log on and enter the game$/) do |name|
  @temp_user = { acct_name: name, acct_password: 'password' }
  ensure_standard_account_exists(@temp_user[:acct_name], @temp_user[:acct_password])
  @temp_connection = create_telnet_connection
  telnet_command(@temp_connection, '', /Login: /, { 'Timeout' => 120 })
  telnet_commands(@temp_connection, [[@temp_user[:acct_name], /Password: /],
                                     [@temp_user[:acct_password], /Command: /]])
  resp = telnet_command(@temp_connection, '1', / ->/)
  expect(resp).to include('Experience :')
end

When(/^I cause the secondary player to "([^"]*)"$/) do |command|
  resp = telnet_command(@temp_connection, command, / ->/)
  expect(resp).to include('Experience :')
end

Then(/^I saw a main menu with version "([^"]*)"$/) do |server_version|
  expect(@last_resp).to match(/^.* \(#{server_version}\) Main Menu$/)
end

Then(/^I saw a main menu with application name "([^"]*)"$/) do |app_name|
  expect(@last_resp).to match(/^.*#{app_name} \(.*\) Main Menu$/)
end

Then(/^I saw an account menu with version "([^"]*)"$/) do |server_version|
  expect(@last_resp).to match(/^.* \(#{server_version}\) Account Menu$/)
end

Then(/^I saw an account menu with application name "([^"]*)"$/) do |app_name|
  expect(@last_resp).to match(/^.*#{app_name} \(.*\) Account Menu$/)
end

Then(/^I saw a character menu with version "([^"]*)"$/) do |server_version|
  expect(@last_resp).to match(/^.* \(#{server_version}\) Character Menu$/)
end

Then(/^I saw a character menu with application name "([^"]*)"$/) do |app_name|
  expect(@last_resp).to match(/^.*#{app_name} \(.*\) Character Menu$/)
end

Then(/^I see a welcome message with version "([^"]*)"$/) do |server_version|
  telnet_command(@connection, '', /Welcome to .* #{server_version}/, { 'Timeout' => 30 })
end

Then(/^I see a welcome message with name "([^"]*)"$/) do |server_name|
  telnet_command(@connection, '', /Welcome to #{server_name}/, { 'Timeout' => 30 })
end

Then(/^I see a news message of "([^"]*)"$/) do |server_news|
  telnet_command(@connection, '', /#{server_news}/, { 'Timeout' => 30 })
end

Then(/^I saw a welcome back with application name "([^"]*)"$/) do |app_name|
  expect(@last_resp).to match(/Your last visit to #{app_name} was on/)
end

When(/^I go to the account menu from the main menu$/) do
  # ODDITY: The account menu doesn't say "Command:" at the end.
  resp = telnet_command(@connection, '4', /Send Mail/, { 'Timeout' => 120 })
  expect(resp).to match(/^.* Account Menu$/)
end

When(/^I go to the character menu from the main menu$/) do
  resp = telnet_command(@connection, '6', /Command:/, { 'Timeout' => 120 })
  expect(resp).to match(/^.* Character Menu$/)
end

Then(/^I enter the game$/) do
  resp = telnet_command(@connection, '1', / ->/)
  expect(resp).to include('Experience :')
end

Then(/^I exit the game$/) do
  resp = telnet_command(@connection, 'quit', /commands\./)
  expect(resp).to include('Type /help for a list of commands.')
end

Then(/^I enter the game from chat$/) do
  resp = telnet_command(@connection, '/play', / ->/)
  expect(resp).to include('Experience :')
end
