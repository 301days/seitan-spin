# frozen_string_literal: true

When(/^I try to ignore the developer account$/) do
  resp = telnet_command(@connection, '/ignore dev01_name', /./)
  expect(resp).to include('ignore')
end

Then(/^I saw an ignore rejection with application name "([^"]*)"$/) do |app_name|
  expect(@last_resp).to match(/^You cannot ignore a #{app_name} staff member.$/)
end

When(/^I issue the lockserver command$/) do
  resp = telnet_command(@connection, '/lockserver', /./)
  expect(resp).to include('Game world has been locked')
end

When(/^I issue the userlist command$/) do
  resp = telnet_command(@connection, '/users', /./)
  expect(resp).to match(/ *Player Name +Location +Stats/)
end

When(/^I issue the help command$/) do
  resp = telnet_command(@connection, '/help', /./)
  expect(resp).to include('Conference Room Commands')
end

When(/^I issue the high scores command$/) do
  resp = telnet_command(@connection, '/scores all', /./)
  expect(resp).to include('Top 10 All Classes of')
end

When(/^I issue the display combat damage command$/) do
  resp = telnet_command(@connection, '/displaycombatdamage', /./)
  expect(resp).to include('combat damage statistics.')
end

Then(/^I saw a user list with application name "([^"]*)"$/) do |app_name|
  expect(@last_resp).to match(/^ *There .* in #{app_name}.$/)
end

Then(/^I saw conference help with application name "([^"]*)"$/) do |app_name|
  expect(@last_resp).to match(%r{^ */exit - Disconnect from #{app_name}.$})
end

Then(/^I saw a high score list with application name "([^"]*)"$/) do |app_name|
  expect(@last_resp).to match(/^ *Top 10 All Classes of #{app_name}$/)
end

Then(/^I saw a message telling me combat damage display was disabled$/) do
  expect(@last_resp).to include('Combat damage statistic information is currently disabled')
end
Then(/^I did not see a message telling me combat damage display was disabled$/) do
  expect(@last_resp).not_to include('Combat damage statistic information is currently disabled')
end
