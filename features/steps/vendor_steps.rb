# frozen_string_literal: true

Given(/^I add a vendor "([^"]*)"$/) do |name|
  vendor = { name: name, npcID: lookup_next_npc_id }
  db_insert_shop_vendor(vendor)
end

Given(/^I give vendor "([^"]*)" the following items:$/) do |vendor_name, table|
  table.hashes.each do |item|
    debug_msg item.inspect
    item[:itemID] = item['itemID']
    item[:notes] = item['notes']
    item[:original] = item['original'].downcase == 'true' ? 1 : 0
    item[:stocked] = item['stocked'].to_i
    item[:restock] = item['restock'].to_i
    item[:NPCIdent] = lookup_next_npc_id
    debug_msg "Giving #{vendor_name} item #{item[:notes]}"
    db_insert_stores(item)
  end
end

Then(/^the log shows "([^"]*)" items cleared from stores$/) do |items_cleared|
  expect(log_contains("Deleted #{items_cleared} store records.", 'SystemGo')).to be_truthy
end

Then(/^the log shows "([^"]*)" items restocked in stores$/) do |items_restocked|
  expect(log_contains("Restocked #{items_restocked} store records.", 'SystemGo')).to be_truthy
end

Then(/^the log does not show any items restocked in stores$/) do
  expect(log_not_this_before_that('Restocked % store records.', 'SystemGo',
                                  'Starting main game loop.', 'SystemGo')).to be_truthy
end

Then(/^the log does not show any items cleared from stores$/) do
  expect(log_not_this_before_that('Deleted % store records.', 'SystemGo',
                                  'Starting main game loop.', 'SystemGo')).to be_truthy
end
