# frozen_string_literal: true

require 'rspec'

Given(/^I use the "([^"]*)" database as-is$/) do |database_name|
  verify_containers_up

  # Kill the server if it's already running
  stdout, stderr, status = Open3.capture3('docker compose stop seitan-spin-gameserver-1')
  debug_msg "Result of docker compose stop seitan-spin-gameserver-1: #{status.inspect}"
  debug_msg "  stdout: #{stdout}" if stdout
  debug_msg "  stderr: #{stderr}" if stderr

  @server_database = database_name
  load_db_if_absent(@server_database)
  set_db_in_config(@server_database)
  # We will clear the log for tests that are looking for something there
  client = connect_to_db(@server_database)
  result = client.execute("DELETE FROM [#{@server_database}].[dbo].[Log]")
  debug_msg "Rows deleted from [Log]: #{result.do}"
end

Given(/^I use the "([^"]*)" database$/) do |database_name|
  # Kill the server if it's already running
  take_containers_down

  @server_database = database_name

  # Bring up the containers
  verify_containers_up

  set_db_in_config(@server_database)
end

Given(/^the server is started$/) do
  expect(start_server).to be_truthy
  expect(log_contains('Starting main game loop.')).to be_truthy
end

# Convenience step, assumed @user is already set up
Given(/^I start the server and play$/) do
  expect(start_server).to be_truthy
  expect(log_contains('Starting main game loop.')).to be_truthy
  ensure_standard_account_exists(@user[:acct_name], @user[:acct_password])
  @connection = create_telnet_connection
  telnet_command(@connection, '', /Login: /, { 'Timeout' => 120 })
  telnet_commands(@connection, [[@user[:acct_name], /Password: /],
                                [@user[:acct_password], /Command: /]])
  resp = telnet_command(@connection, '1', / ->/)
  expect(resp).to include('Experience :')
end

Given(/^the server executable is started$/) do
  expect(start_server).to be_truthy
end

And(/^I allow time for the server to complete startup$/) do
  expect(log_contains('Starting main game loop.')).to be_truthy
end

And(/^I do not allow time for the server to complete startup$/) do
  expect(log_contains_immediate('Starting main game loop.')).to be_falsy
end

When('I wait for {int} seconds {string}') do |seconds, _explanation|
  sleep(seconds)
end
