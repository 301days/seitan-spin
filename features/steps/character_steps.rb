# frozen_string_literal: true

require 'timeout'

Given(/^I give the character the spell "([^"]*)"$/) do |spell_name|
  spell_id = get_spell_id(spell_name)
  ensure_player_has_spell(get_player_id(@user[:char_name]), spell_id)
end

Given(/^I put a spellbook in the character's right hand$/) do
  item_id = lookup_item_id(name: 'spellbook')
  put_attuned_in_player_hand(get_player_id(@user[:char_name]), item_id, :right)
end

Given(/^I put a gold ring in the character's left hand$/) do
  item_id = lookup_item_id(identifiedName: 'Gold Ring')
  put_in_player_hand(get_player_id(@user[:char_name]), item_id, :left)
end

Given(/^I put a naphtha in the character's (right|left) hand$/) do |hand|
  item_id = lookup_item_id(effectType: EFFECT_TYPE_ENUM.index(:naphtha))
  put_in_player_hand(get_player_id(@user[:char_name]), item_id, hand.to_sym)
end

Given(/^I put a blindness cure in the character's (right|left) hand$/) do |hand|
  item_id = lookup_item_id(effectType: EFFECT_TYPE_ENUM.index(:blindness_cure))
  put_in_player_hand(get_player_id(@user[:char_name]), item_id, hand.to_sym)
end

Given(/^I put a coffee in the character's (right|left) hand$/) do |hand|
  item_id = lookup_item_id(effectType: EFFECT_TYPE_ENUM.index(:coffee))
  put_in_player_hand(get_player_id(@user[:char_name]), item_id, hand.to_sym)
end

Given(/^I put a drake potion in the character's (right|left) hand$/) do |hand|
  item_id = lookup_item_id(effectType: EFFECT_TYPE_ENUM.index(:drake_potion))
  put_in_player_hand(get_player_id(@user[:char_name]), item_id, hand.to_sym)
end

# NOTE: currently only works for potions with _only_ that effect, not multiple effects.
Given(/^I put (a|an) "([^"]*)" potion in the character's (right|left) hand$/) \
     do |_aan, effect, hand|
  item_id = lookup_item_id(effectType: EFFECT_TYPE_ENUM.index(effect.downcase.to_sym).to_s)
  put_in_player_hand(get_player_id(@user[:char_name]), item_id, hand.to_sym)
end

Given(/^I create (a|an) "([^"]*)" potion of amount "([^"]*)" and duration "([^"]*)"$/) \
     do |_aan, effect, amount, duration|
  @item_id = create_item_from_blank(
    { itemType: 'Potable', baseType: 'Bottle', effectType: '' },
    { effectType: EFFECT_TYPE_ENUM.index(effect.downcase.to_sym).to_s,
      effectAmount: amount, effectDuration: duration }
  )
end

Given(/^I give the item (a|an) "([^"]*)" of "([^"]*)"$/) do |_aan, field, value|
  new_hash = { field.to_sym => value }
  @item_id = create_item_from_blank({ itemID: @item_id }, new_hash)
end

Given(/^I put the item in the character's (right|left) hand$/) do |hand|
  put_in_player_hand(get_player_id(@user[:char_name]), @item_id, hand.to_sym)
end

Given(/^I put poison berries in the character's (right|left) hand$/) do |hand|
  item_id = lookup_item_id({ special: 'poisonberry' })
  put_in_player_hand(get_player_id(@user[:char_name]), item_id, hand.to_sym)
end

Given(/^I put a stone halberd in the character's (right|left) hand$/) do |hand|
  item_id = lookup_item_id({ identifiedName: 'Stone Halberd' })
  put_in_player_hand(get_player_id(@user[:char_name]), item_id, hand.to_sym)
end

When(/^I look at "([^"]*)"$/) do |item|
  telnet_command(@connection, "look at #{item}", / ->/)
end

When(/^I eat "([^"]*)"$/) do |item|
  telnet_command(@connection, "eat #{item}", / ->/)
end

When(/^I open and drink "([^"]*)"$/) do |item|
  telnet_command(@connection, "open #{item}; drink #{item}", / ->/)
end

When(/^I open "([^"]*)"$/) do |item|
  telnet_command(@connection, "open #{item}", / ->/)
end

When(/^I drink "([^"]*)"$/) do |item|
  telnet_command(@connection, "drink #{item}", / ->/)
end

When(/^I drop "([^"]*)"$/) do |item|
  telnet_command(@connection, "drop #{item}", / ->/)
end

When(/^I throw a bottle in my cell$/) do
  telnet_command(@connection, 'throw bottle n s', / ->/)
end

When(/^I warm the spell "([^"]*)"$/) do |spell_name|
  resp = telnet_command(@connection, 'na na na na', / ->/)
  expect(resp).to include("You warm the spell #{spell_name}.")
end

When(/^I read the book$/) do
  resp = telnet_command(@connection, 'read book', / ->/)
  expect(resp).to include('The incantation for')
end

When(/^I cast the warmed spell$/) do
  telnet_command(@connection, 'cast', / ->/)
end

Then(/^I have a recall ring in my left hand$/) do
  resp = telnet_command(@connection, 'look in left hand', / ->/)
  expect(resp).to include('You are looking at a small gold ring. ' \
                          'It is emitting a faint blue glow.')
end

Then(/^I do not have a recall ring in my "([^"]*)" hand$/) do |which_hand|
  resp = telnet_command(@connection, "look in #{which_hand} hand", / ->/)
  expect(resp).not_to include('You are looking at a small gold ring. ' \
                              'It is emitting a faint blue glow.')
end

Then(/^My spellbook exploded$/) do
  expect(@last_resp).to include('Your spellbook explodes!')
end

Given(/^I move the player to the Ancestor Start cell$/) do
  #   On map test01 this is 50,27,0
  move_player_to(get_player_id(@user[:char_name]), 50, 27, 0)
end

Given(/^I move the player to the Underworld Portal cell$/) do
  #   On map test01 this is 50,31,0
  move_player_to(get_player_id(@user[:char_name]), 50, 31, 0)
end

# We look to make the command interpreter actually do something, so the server can skip to the
#  next round if it wants to.
When(/^I speak "([^"]*)"$/) do |utterance|
  telnet_command(@connection, "look / #{utterance}", / ->/)
end

When(/^I chant "([^"]*)"$/) do |utterance|
  telnet_command(@connection, utterance, / ->/)
end

Then(/^I saw (a|the) message "([^"]*)"$/) do |_athe, message|
  expect(@last_resp).to include(message)
end

Then(/^I did not see (a|the) message "([^"]*)"$/) do |_athe, message|
  expect(@last_resp).not_to include(message)
end

Given(/^I set the character's alignment to (lawful|neutral|evil)$/) do |desired_alignment|
  set_character_alignment(get_player_id(@user[:char_name]), desired_alignment.to_sym)
end

Given(/^I set the character's current karma to "([^"]*)"$/) do |desired_karma|
  set_character_karma(get_player_id(@user[:char_name]), desired_karma.to_i)
end

Given(/^I set the character's "([^"]*)" stat to "([^"]*)"$/) do |stat, desired_value|
  set_character_stat(get_player_id(@user[:char_name]), stat, desired_value.to_i)
end

Given(/^I set the character's "([^"]*)" skill to "([^"]*)"$/) do |skill, desired_value|
  set_character_skill(get_player_id(@user[:char_name]), skill, desired_value.to_i)
end

Given(/^I set the character's max HP to "([^"]*)"$/) do |desired_hp|
  set_character_max_hp(get_player_id(@user[:char_name]), desired_hp.to_i)
end

Given(/^I set the character's current HP to "([^"]*)"$/) do |desired_hp|
  set_character_hp(get_player_id(@user[:char_name]), desired_hp.to_i)
end

Given(/^I set the character's current and max HP to "([^"]*)"$/) do |desired_hp|
  set_character_max_hp(get_player_id(@user[:char_name]), desired_hp.to_i)
  set_character_hp(get_player_id(@user[:char_name]), desired_hp.to_i)
end

Given(/^I set the character's max stamina to "([^"]*)"$/) do |desired_value|
  set_character_max_stamina(get_player_id(@user[:char_name]), desired_value.to_i)
end

Given(/^I set the character's current stamina to "([^"]*)"$/) do |desired_value|
  set_character_stamina(get_player_id(@user[:char_name]), desired_value.to_i)
end

Given(/^I set the character's max mana to "([^"]*)"$/) do |desired_value|
  set_character_max_mana(get_player_id(@user[:char_name]), desired_value.to_i)
end

Given(/^I set the character's current mana to "([^"]*)"$/) do |desired_value|
  set_character_mana(get_player_id(@user[:char_name]), desired_value.to_i)
end

Given(/^I set the character's current constitution to "([^"]*)"$/) do |desired_value|
  set_character_constitution(get_player_id(@user[:char_name]), desired_value.to_i)
end

Given(/^I set the character's current age to "([^"]*)"$/) do |desired_value|
  set_character_age(get_player_id(@user[:char_name]), desired_value.to_i)
end

# This doesn't work; apparently I can't poison a character when they're offline.
Given(/^I poison the character$/) do
  add_player_effect(get_player_id(@user[:char_name]),
                    EFFECT_TYPE_ENUM.index(:poison), 5, 10)
end

Given(/^I bless the character for "([^"]*)" turns$/) do |num_turns|
  add_player_effect(get_player_id(@user[:char_name]),
                    EFFECT_TYPE_ENUM.index(:bless), 5, num_turns.to_i)
end

Given(/^I inflict fear on the character for "([^"]*)" turns$/) do |num_turns|
  add_player_effect(get_player_id(@user[:char_name]),
                    EFFECT_TYPE_ENUM.index(:fear), 5, num_turns.to_i)
end

Given('I blind the character for {string} turns') do |num_turns|
  add_player_effect(get_player_id(@user[:char_name]),
                    EFFECT_TYPE_ENUM.index(:blind), 5, num_turns.to_i)
end

Given(/^I blind the character for "{int}" turns$/) do |num_turns|
  add_player_effect(get_player_id(@user[:char_name]),
                    EFFECT_TYPE_ENUM.index(:blind), 5, num_turns.to_i)
end

Given('I give the character a {string} effect of {string} for {string} turns') do |effect,
  effect_value, num_turns|
  add_player_effect(get_player_id(@user[:char_name]),
                    EFFECT_TYPE_ENUM.index(effect.downcase.to_sym),
                    effect_value.to_i, num_turns.to_i)
end

Given('I give the character an {string} effect of {string} for {string} turns') do |effect,
  effect_value, num_turns|
  add_player_effect(get_player_id(@user[:char_name]),
                    EFFECT_TYPE_ENUM.index(effect.downcase.to_sym),
                    effect_value.to_i, num_turns.to_i)
end

Given(/^I give the character (a|an) "([^"]*)" effect of "([^"]*)" for "{int}" turns$/) \
     do |_aan, effect, effect_value, num_turns|
  add_player_effect(get_player_id(@user[:char_name]),
                    EFFECT_TYPE_ENUM.index(effect.downcase.to_sym),
                    effect_value.to_i, num_turns.to_i)
end

When('I wait for {string} to appear') do |waited_for|
  telnet_command(@connection, "l \"I'm waiting", / ->/) until @last_resp.include?(waited_for)
end

When('I wait for {string} turns') do |turns|
  (1..(turns.to_i)).each do |turn|
    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
end

When(/^I wait for "{int}" turns?$/) do |turns|
  (1..(turns.to_i)).each do |turn|
    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
end

Then('within {string} turns I see a message {string}') do |max_turns, waited_for|
  (1..(max_turns.to_i)).each do |turn|
    break if @last_resp.include?(waited_for)

    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
  expect(@last_resp.include?(waited_for)).to be_truthy
end

Then('within {string} turns I see the message {string}') do |max_turns, waited_for|
  (1..(max_turns.to_i)).each do |turn|
    break if @last_resp.include?(waited_for)

    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
  expect(@last_resp.include?(waited_for)).to be_truthy
end

Then(/^within "{int}" turns I see (a|the) message "([^"]*)"$/) do |max_turns, _athe, waited_for|
  (1..(max_turns.to_i)).each do |turn|
    break if @last_resp.include?(waited_for)

    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
  expect(@last_resp.include?(waited_for)).to be_truthy
end

Then(/^within "{int}" turns I no longer see (a|the) message "([^"]*)"$/) \
    do |max_turns, _athe, waited_for|
  (1..(max_turns.to_i)).each do |turn|
    break unless @last_resp.include?(waited_for)

    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
  expect(@last_resp).to_not include(waited_for)
end

Then('within {string} turns I no longer see the message {string}') \
    do |max_turns, waited_for|
  (1..(max_turns.to_i)).each do |turn|
    break unless @last_resp.include?(waited_for)

    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
  expect(@last_resp).to_not include(waited_for)
end

Then(/^within "([^"]*)" turns I see (a|the) message "([^"]*)" only once$/) \
    do |max_turns, _athe, waited_for|
  sightings = 0
  (1..(max_turns.to_i)).each do |turn|
    sightings += @last_resp.scan(waited_for).size
    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
  expect(sightings).to be_equal(1)
end

Then(/^after "([^"]*)" turns I see (a|the) message "([^"]*)"$/) \
    do |max_turns, _athe, waited_for|
  (1..(max_turns.to_i)).each do |turn|
    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
  expect(@last_resp.include?(waited_for)).to be_truthy
end

Then(/^within "([^"]*)" turns I never see (a|the) message "([^"]*)"$/) \
    do |max_turns, _athe, waited_for|
  (1..(max_turns.to_i)).each do |turn|
    break if @last_resp.include?(waited_for)

    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
  expect(@last_resp.include?(waited_for)).to be_falsy
end

Then(/^after "([^"]*)" turns I no longer see (a|the) message "([^"]*)"$/) \
    do |max_turns, _athe, waited_for|
  (1..(max_turns.to_i)).each do |turn|
    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
  expect(@last_resp.include?(waited_for)).to be_falsy
end

When(/^after "([^"]*)" turns I have a current HP of "([^"]*)"$/) do |max_turns, desired_value|
  (1..(max_turns.to_i)).each do |turn|
    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
  waited_for = %r{hits\s*:\s*#{desired_value}/}i
  expect(@last_resp).to match(waited_for)
end

When(/^after "([^"]*)" turns I have a current stamina of "([^"]*)"$/) do |max_turns, desired_value|
  (1..(max_turns.to_i)).each do |turn|
    telnet_command(@connection, "l \"I'm waiting #{turn}", / ->/)
  end
  waited_for = "Stamina    : #{desired_value}"
  expect(@last_resp.include?(waited_for)).to be_truthy
end

Then(/^I have a current HP of "([^"]*)"$/) do |desired_value|
  waited_for = %r{hits\s*:\s*#{desired_value}/}i
  expect(@last_resp).to match(waited_for)
end

Then(/^I have a current and max HP of "([^"]*)"$/) do |desired_value|
  waited_for = %r{hits\s*:\s*#{desired_value}/#{desired_value}\b}i
  expect(@last_resp).to match(waited_for)
end

Then(/^I have a current mana of "([^"]*)"$/) do |desired_value|
  waited_for = /magic points\s*:\s*#{desired_value}\b/i
  expect(@last_resp).to match(waited_for)
end

When(/^I have a current stamina of "([^"]*)"$/) do |desired_value|
  waited_for = "Stamina    : #{desired_value}"
  expect(@last_resp).to include(waited_for)
end

When(/^I attack the "([^"]*)"$/) do |enemy|
  Timeout.timeout(30) do
    telnet_command(@connection, "attack #{enemy}", / ->/) until @last_resp.include?('Swing hits')
  end
end

Then(/^I saw numerical combat damage$/) do
  expect(@last_resp).to match(/damage! \(\d+\)/)
end

When(/^I rest$/) do
  telnet_command(@connection, 'rest', / ->/, { 'Timeout' => 60 })
end

When(/^I move east$/) do
  telnet_command(@connection, 'e', / ->/, { 'Timeout' => 60 })
end

When(/^I move "([^"]*)"$/) do |path|
  telnet_command(@connection, path.to_s, / ->/)
end

Then(/^I have a constitution stat of "([^"]*)"$/) do |desired_value|
  resp = telnet_command(@connection, 'show stats', / ->/)
  waited_for = /constitution\s*:\s*#{desired_value}\b/i
  expect(resp).to match(waited_for)
end

Then(/^I have a "([^"]*)" stat of "([^"]*)"$/) do |stat, desired_value|
  resp = telnet_command(@connection, 'show stats', / ->/)
  waited_for = /#{stat}\s*:\s*#{desired_value}\b/i
  expect(resp).to match(waited_for)
end

Then(/^I have a temporary "([^"]*)" stat of "([^"]*)"$/) do |stat, desired_value|
  resp = telnet_command(@connection, 'show tempstats', / ->/)
  waited_for = /#{stat}\s*:\s*#{desired_value}\b/i
  expect(resp).to match(waited_for)
end

Then(/^I have an age of "([^"]*)"$/) do |desired_value|
  resp = telnet_command(@connection, 'show stats', / ->/)
  waited_for = /You are an?\s+#{desired_value}\b/i
  expect(resp).to match(waited_for)
end

Then(/^I have a shielding of "([^"]*)"$/) do |desired_value|
  resp = telnet_command(@connection, 'show effects', / ->/)
  waited_for = /\bTotal Shielding = #{desired_value}\b/i
  expect(resp).to match(waited_for)
end

Then(/^I have a "([^"]*)" resistance of "([^"]*)"$/) do |type, desired_value|
  resp = telnet_command(@connection, 'show resists', / ->/)
  waited_for = /#{type}\s*:\s*#{desired_value}\b/i
  expect(resp).to match(waited_for)
end

Then(/^I have a "([^"]*)" protection of "([^"]*)"$/) do |type, desired_value|
  resp = telnet_command(@connection, 'show protection', / ->/)
  waited_for = /#{type}\s*:\s*#{desired_value}\b/i
  expect(resp).to match(waited_for)
end

Then(/^I saw myself$/) do
  # TODO: make sure this is an actual character, not part of some other text.
  expect(@last_resp).to match(/\b#{@user[:char_name]}\b/)
end

Then(/^I did not see myself$/) do
  # TODO: make sure this is an actual character, not part of some other text.
  expect(@last_resp).to_not match(/\b#{@user[:char_name]}\b/)
end

Then(/^I see my doppelganger in a trance$/) do
  # The gsub is in case this text is split between lines
  resp = telnet_command(@connection, "look at #{@user[:char_name]}", / ->/).gsub(/\s+/, ' ')
  waited_for = /\b#{@user[:char_name]} is in a trance./i
  expect(resp).to match(waited_for)
end

Then(/^I see "([^"]*)" in a trance$/) do |target|
  # The gsub is in case this text is split between lines
  resp = telnet_command(@connection, "look at #{target}", / ->/).gsub(/\s+/, ' ')
  waited_for = /\b#{target} is in a trance./i
  expect(resp).to match(waited_for)
end

When(/^I saw (a|an) "([^"]*)"$/) do |_aan, target|
  # TODO: make sure this is an actual character, not part of some other text.
  expect(@last_resp).to match(/\b#{target}\b/)
end
