# frozen_string_literal: true

require 'nokogiri'
require 'tiny_tds'

def string_or_null(value)
  value ? "N'#{value}'" : 'NULL'
end

def db_connect_hash(server_database)
  connect_hash = { username: ENV.fetch('DB_USERNAME', nil), password: ENV.fetch('DB_PASSWORD', nil),
                   dataserver: ENV.fetch('DB_DATASERVER', nil), database: server_database }
  if ENV['DB_HOST_OVERRIDE']
    connect_hash[:host] = ENV['DB_HOST_OVERRIDE']
    connect_hash[:port] = Integer(ENV.fetch('DB_PORT_OVERRIDE', nil))
    connect_hash.delete(:dataserver)
  end
  # debug_msg "Will connect to DB using #{connect_hash}"
  connect_hash
end

def load_db_if_absent(server_database)
  client = connect_to_db(server_database)
  debug_msg "Result of DB connect: #{client}"
  debug_msg "Result of DB connect: #{client}"
rescue TinyTds::Error => e
  debug_msg "load_db_if_absent #{e.inspect}"
  fail "#{server_database} was absent!"
end

def set_db_in_config(server_database)
  set_value_in_configfile('SQL_CONNECTION',
                          "User ID='#{ENV.fetch('DB_USERNAME',
                                                nil)}';Password='#{ENV.fetch('DB_PASSWORD',
                                                                             nil)}';" \
                          "Initial Catalog='#{server_database}';" \
                          "Data Source='#{ENV.fetch('DB_DATASERVER', nil)}';" \
                          'Connect Timeout=15')
end

def connect_to_db(database)
  debug_msg "connect_to_db #{database.inspect}"
  @db_client ||= nil
  @db_hash ||= {}
  debug_msg "@db_hash is #{@db_hash}"
  new_hash = db_connect_hash(database)
  if new_hash.keys.all? { |key| @db_hash.key?(key) && new_hash[key] == @db_hash[key] }
    # if db_connect_hash(database) == @db_hash
    debug_msg 'Same hash'
    if @db_client&.active?
      debug_msg 'Already connected!'
      return @db_client
    end
  end
  debug_msg "new_hash is #{new_hash}"
  @db_hash = new_hash
  try = 0
  begin
    @db_client = TinyTds::Client.new(@db_hash)
  rescue TinyTds::Error => e
    debug_msg "#{try} connect_to_db #{e.inspect}"
    try += 1
    try <= 5 ? retry : raise
  end
end

def find_or_create_account(acct_name, acct_password)
  client = connect_to_db(@server_database)
  # Nothing prevents identical users, so we need to check if it's already there:
  accountID = nil
  result = client.execute("SELECT * FROM [#{@server_database}].[dbo].[Account] \
                          WHERE [account] = '#{acct_name}'")
  result.each do |row|
    accountID = row['accountID']
  end
  fail 'duplicate users!' if result.affected_rows > 1

  if accountID.nil?
    query = "INSERT INTO [#{@server_database}].[dbo].[Account] \
      ([account],[password],[email]) VALUES \
      ('#{acct_name}', '#{acct_password}', '#{acct_name}@test.not')"
    debug_msg query
    result = client.execute(query)
    affected_rows = result.do
    fail 'Failed to insert user!' if affected_rows != 1
  end
  result = client.execute("SELECT * FROM [#{@server_database}].[dbo].[Account] \
                          WHERE [account] = '#{acct_name}'")
  result.each do |row|
    accountID = row['accountID']
  end
  fail 'duplicate users!' if result.affected_rows > 1

  accountID
end

def ensure_developer_account_exists(acct_name, acct_password)
  accountID = find_or_create_account(acct_name, acct_password)
  @user[:char_name] = "#{acct_name}_name"
  # now that we have an accountID we can make the player
  desired_player = { accountID: accountID, account: acct_name, name: @user[:char_name],
                     impLevel: 4 }
  return if player_exists(desired_player)

  client = connect_to_db(@server_database)
  # first delete any players already attached to this accountID
  query = "DELETE \
      FROM [#{@server_database}].[dbo].[Player]
      WHERE [accountID] = #{accountID}"
  result = client.execute(query)
  affected_rows = result.do
  debug_msg "Pre-existing players for account ID #{accountID} deleted: #{affected_rows}"
  # now insert the new player with DEV access
  db_insert_player({ accountID: accountID, account: acct_name, name: @user[:char_name],
                     impLevel: 4 })
end

def ensure_standard_account_exists(acct_name, acct_password)
  accountID = find_or_create_account(acct_name, acct_password)
  @user[:char_name] = "#{acct_name}_name"
  # now that we have an accountID we can make the player
  desired_player = { accountID: accountID, account: acct_name, name: @user[:char_name] }
  return if player_exists(desired_player)

  client = connect_to_db(@server_database)
  # first delete any players already attached to this accountID
  query = "DELETE \
      FROM [#{@server_database}].[dbo].[Player]
      WHERE [accountID] = #{accountID}"
  result = client.execute(query)
  affected_rows = result.do
  debug_msg "Pre-existing players for account ID #{accountID} deleted: #{affected_rows}"
  # now insert the new player with standard access
  db_insert_player({ accountID: accountID, account: acct_name, name: @user[:char_name] })
end

def disable_spawn_zones
  client = connect_to_db(@server_database)
  result = client.execute("UPDATE [#{@server_database}].[dbo].[SpawnZone] SET [enabled] = 0")
  affected_rows = result.do
  fail 'Failed to disable spawn zones!' if affected_rows != 1

  affected_rows
end
