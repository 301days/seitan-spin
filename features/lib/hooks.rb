# frozen_string_literal: true

require 'open3'
require 'tiny_tds'
require 'timeout'

# Before do |scenario|
#   debug_msg "Scenario supports: #{(scenario.methods - Object.methods).inspect}"
# end

Before('@kill_server_after') do
  verify_containers_up
end

Before('@reset_server') do
  # Kill the server if it's already running
  take_containers_down

  @server_database = 'minimal'
  # Bring up the containers
  verify_containers_up

  # Start up the server
  set_db_in_config(@server_database)
  start_server
  # Wait for server to come up
  connect_to_db(@server_database)
  log_contains('Starting main game loop.')
end

After('@reset_server') do
  # Kill the server if it's running
  take_containers_down
end

After('@kill_server_after') do
  if take_containers_down
    stdout, stderr, status = Open3.capture3('rm -r gameserver/*')
    debug_msg "Result of rm -r gameserver/*: #{status.inspect}"
    debug_msg "  stdout: #{stdout}" if stdout
    debug_msg "  stderr: #{stderr}" if stderr
  end
  status.success?
end
