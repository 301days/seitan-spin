# frozen_string_literal: true

# Plan to build this out over time as necessary
DEFAULT_STORES_ITEM = {
  NPCIdent: 0,
  notes: 'Test item',
  original: 0,
  itemID: 0,
  sellPrice: 1,
  stocked: 10,
  charges: -1,
  figExp: 0,
  seller: 'SellingGuy', # can be NULL
  restock: 10
}.freeze

def db_insert_stores(db_hash)
  # anything in the hash overrides defaults
  item = DEFAULT_STORES_ITEM.merge(db_hash)
  client = connect_to_db(@server_database)
  query = "INSERT [#{@server_database}].[dbo].[Stores] ([NPCIdent], [notes], \
    [original], [itemID], [sellPrice], [stocked], [charges], [figExp], [seller], [restock]) \
    VALUES ( \
    #{item[:NPCIdent]}, \
    N'#{item[:notes]}', \
    #{item[:original]}, \
    #{item[:itemID]}, \
    #{item[:sellPrice]}, \
    #{item[:stocked]}, \
    #{item[:charges]}, \
    #{item[:figExp]}, \
    #{string_or_null(item[:seller])}, \
    #{item[:restock]} \
    )"
  debug_msg "query: #{query}"
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to insert item!' if affected_rows != 1
end

# INSERT [dbo].[Stores] ([stockID], [NPCIdent], [notes], [original], [itemID], [sellPrice],
#  [stocked], [charges], [figExp], [seller], [restock]) VALUES (1, 181, N'Kesmai Pawn Shop', 1,
#  21031, 350, 2, -1, 0, NULL, 2)
