# frozen_string_literal: true

DEFAULT_NPC = {
  npcID: 0,
  name: 'test NPC'
}.freeze
DEFAULT_VENDOR = {
  name: 'test vendor',
  merchantType: 'Pawn',
  merchantMarkup: 3
}.freeze

DEFAULT_SPAWNZONE = {
  enabled: 0,
  npcID: 0,
  spawnTimer: 30,
  maxAllowedInZone: 1,
  minZone: 0,
  maxZone: 0,
  spawnLand: 0,
  spawnMap: 0,
  spawnX: 42,
  spawnY: 27,
  spawnZ: 0,
  spawnRadius: 0
}.freeze

def lookup_npc_id(lookup_hash)
  client = connect_to_db(@server_database)
  query = db_field_from_hash('NPC', lookup_hash, 'npcID')
  debug_msg "Query is #{query}"
  result = client.execute(query)
  row = result.each(first: true)
  debug_msg row.inspect
  fail 'Unable to get npcID!' if result.affected_rows != 1

  row[0]['npcID']
end

def lookup_next_npc_id
  client = connect_to_db(@server_database)
  query = 'SELECT MAX(npcID) AS npcID FROM [dbo].[NPC]'
  debug_msg "Query is #{query}"
  result = client.execute(query)
  row = result.each(first: true)
  debug_msg row.inspect
  fail 'Unable to get maximum npcID!' if result.affected_rows != 1

  row[0]['npcID'] + 1
end

def db_insert_spawn_zone(db_hash)
  spawn_zone = DEFAULT_SPAWNZONE.merge(db_hash)
  debug_msg "Inserting spawn zone: #{spawn_zone.inspect}"
  client = connect_to_db(@server_database)
  query = db_insert_from_hash('SpawnZone', spawn_zone)
  debug_msg "Query is #{query}"
  result = client.execute(query)
  debug_msg "Result is #{result.inspect} and result.insert is #{result.insert}"
  affected_rows = result.do
  fail 'Unable to insert spawn zone!' if affected_rows != 1

  result.insert # zone ID
end

def db_remove_spawn_zone(zone_id)
  debug_msg "Removing spawn zone: #{zone_id}"
  client = connect_to_db(@server_database)
  query = "DELETE FROM [dbo].[SpawnZone] WHERE zoneID = #{zone_id}"
  debug_msg "Query is #{query}"
  result = client.execute(query)
  debug_msg result.inspect
  affected_rows = result.do
  fail 'Unable to delete spawn zone!' if affected_rows != 1
end

def db_insert_shop_vendor(db_hash)
  # anything in the hash overrides defaults
  npc = DEFAULT_NPC.merge(DEFAULT_VENDOR.merge(db_hash))
  client = connect_to_db(@server_database)
  query = "INSERT [dbo].[NPC] ([npcID], [notes], [name], [attackSound], [deathSound], [idleSound], \
    [movementString], [shortDesc], [longDesc], [visualKey], [classFullName], [baseArmorClass], \
    [thac0Adjustment], [special], [npcType], [aiType], [species], [gold], [classType], [exp], \
    [hitsMax], [alignment], [stamina], [manaMax], [speed], [strength], [dexterity], \
    [intelligence], [wisdom], [constitution], [charisma], [lootVeryCommonAmount], \
    [lootVeryCommonArray], [lootVeryCommonOdds], [lootCommonAmount], [lootCommonArray], \
    [lootCommonOdds], [lootRareAmount], [lootRareArray], [lootRareOdds], \
    [lootVeryRareAmount], [lootVeryRareArray], [lootVeryRareOdds], [lootLairAmount], \
    [lootLairArray], [lootLairOdds], [lootAlwaysArray], [lootBeltAmount], [lootBeltArray], \
    [lootBeltOdds], \
    [spawnArmorArray], [spawnLeftHandArray], [spawnLeftHandOdds], [spawnRightHandArray], \
    [mace], [bow], [dagger], [flail], [rapier], [twoHanded], [staff], [shuriken], [sword], \
    [threestaff], [halberd], [thievery], [unarmed], [magic], [bash], [level], [animal], \
    [tanningResult], [undead], [spectral], [hidden], [poisonous], [waterDweller], [fly], \
    [breatheWater], [nightVision], [lair], [lairCells], [mobile], [command], [castMode], \
    [randomName], [attackString1], [attackString2], [attackString3], [attackString4], \
    [attackString5], [attackString6], [blockString1], [blockString2], [blockString3], \
    [merchantType], [merchantMarkup], [trainerType], [interactiveType], [gender], [race], [age], \
    [patrol], [patrolRoute], [immuneFire], [immuneCold], [immunePoison], [immuneLightning], \
    [immuneCurse], [immuneDeath], [immuneStun], [immuneFear], [immuneBlind], [spells], [quests], \
    [groupAmount], [groupMembers], [weaponRequirement], [questFlags]) \
    VALUES ( \
    #{npc[:npcID]}, \
    N'test vendor', \
    N'#{npc[:name]}', \
    NULL, NULL, NULL, \
    N'', N'Fighter', NULL, N'male_merchant', N'Fighter', \
    10, 0, N'', N'Merchant', N'Unknown', N'Humanoid', 0, N'Fighter', 0, 0, N'Lawful', \
    0, 0, 3, 10, 9, 12, 13, 35, 35, \
    0, N'', 0, 0, N'', 0, 0, N'', 0, 0, N'', 0, 0, N'', 0, N'', 0, N'', 0, N'8010 15010', N'0', \
    0, N'', 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 0, 800, 0, 3000, \
    3, 0, N'', 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, 0, \
    N'', N'', N'', N'', N'', N'', N'', N'', N'',
    N'#{npc[:merchantType]}', \
    #{npc[:merchantMarkup]}, \
    N'None', N'None', \
    1, N'Mu', 28800, 0, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 0, NULL, NULL, NULL)"
  result = client.execute(query)
  affected_rows = result.do
  fail 'Unable to insert vendor!' if affected_rows != 1
end

# CREATE TABLE [dbo].[NPC](
# 	[catalogID] [int] IDENTITY(1,1) NOT NULL,
# 	[npcID] [int] NOT NULL,
# 	[notes] [text] NULL,
# 	[name] [nvarchar](255) NOT NULL,
# 	[attackSound] [varchar](50) NULL,
# 	[deathSound] [varchar](50) NULL,
# 	[idleSound] [varchar](50) NULL,
# 	[movementString] [nvarchar](255) NULL,
# 	[shortDesc] [varchar](50) NULL,
# 	[longDesc] [varchar](max) NULL,
# 	[visualKey] [varchar](50) NULL,
# 	[classFullName] [varchar](50) NULL,
# 	[baseArmorClass] [float] NOT NULL,
# 	[thac0Adjustment] [int] NOT NULL,
# 	[special] [nvarchar](255) NULL,
# 	[npcType] [varchar](50) NOT NULL,
# 	[aiType] [varchar](50) NOT NULL,
# 	[species] [varchar](50) NOT NULL,
# 	[gold] [int] NULL,
# 	[classType] [varchar](50) NOT NULL,
# 	[exp] [int] NULL,
# 	[hitsMax] [int] NULL,
# 	[alignment] [varchar](50) NOT NULL,
# 	[stamina] [int] NOT NULL,
# 	[manaMax] [int] NOT NULL,
# 	[speed] [int] NOT NULL,
# 	[strength] [int] NOT NULL,
# 	[dexterity] [int] NOT NULL,
# 	[intelligence] [int] NOT NULL,
# 	[wisdom] [int] NOT NULL,
# 	[constitution] [int] NOT NULL,
# 	[charisma] [int] NOT NULL,
# 	[lootVeryCommonAmount] [int] NOT NULL,
# 	[lootVeryCommonArray] [varchar](1000) NULL,
# 	[lootVeryCommonOdds] [int] NOT NULL,
# 	[lootCommonAmount] [int] NOT NULL,
# 	[lootCommonArray] [varchar](1000) NOT NULL,
# 	[lootCommonOdds] [int] NOT NULL,
# 	[lootRareAmount] [int] NOT NULL,
# 	[lootRareArray] [varchar](1000) NULL,
# 	[lootRareOdds] [int] NOT NULL,
# 	[lootVeryRareAmount] [int] NOT NULL,
# 	[lootVeryRareArray] [varchar](1000) NULL,
# 	[lootVeryRareOdds] [int] NOT NULL,
# 	[lootLairAmount] [int] NOT NULL,
# 	[lootLairArray] [varchar](1000) NULL,
# 	[lootLairOdds] [int] NOT NULL,
# 	[lootAlwaysArray] [varchar](1000) NULL,
# 	[lootBeltAmount] [int] NULL,
# 	[lootBeltArray] [varchar](1000) NULL,
# 	[lootBeltOdds] [int] NOT NULL,
# 	[spawnArmorArray] [varchar](1000) NULL,
# 	[spawnLeftHandArray] [varchar](1000) NULL,
# 	[spawnLeftHandOdds] [int] NULL,
# 	[spawnRightHandArray] [varchar](1000) NULL,
# 	[mace] [bigint] NOT NULL,
# 	[bow] [bigint] NOT NULL,
# 	[dagger] [bigint] NOT NULL,
# 	[flail] [bigint] NOT NULL,
# 	[rapier] [bigint] NOT NULL,
# 	[twoHanded] [bigint] NOT NULL,
# 	[staff] [bigint] NOT NULL,
# 	[shuriken] [bigint] NOT NULL,
# 	[sword] [bigint] NOT NULL,
# 	[threestaff] [bigint] NOT NULL,
# 	[halberd] [bigint] NOT NULL,
# 	[thievery] [bigint] NOT NULL,
# 	[unarmed] [bigint] NOT NULL,
# 	[magic] [bigint] NOT NULL,
# 	[bash] [bigint] NOT NULL,
# 	[level] [smallint] NOT NULL,
# 	[animal] [bit] NOT NULL,
# 	[tanningResult] [varchar](255) NULL,
# 	[undead] [bit] NOT NULL,
# 	[spectral] [bit] NOT NULL,
# 	[hidden] [bit] NOT NULL,
# 	[poisonous] [int] NOT NULL,
# 	[waterDweller] [bit] NOT NULL,
# 	[fly] [bit] NOT NULL,
# 	[breatheWater] [bit] NOT NULL,
# 	[nightVision] [bit] NOT NULL,
# 	[lair] [bit] NOT NULL,
# 	[lairCells] [varchar](255) NULL,
# 	[mobile] [bit] NOT NULL,
# 	[command] [bit] NOT NULL,
# 	[castMode] [int] NOT NULL,
# 	[randomName] [bit] NOT NULL,
# 	[attackString1] [varchar](255) NULL,
# 	[attackString2] [varchar](255) NULL,
# 	[attackString3] [varchar](255) NULL,
# 	[attackString4] [varchar](255) NULL,
# 	[attackString5] [varchar](255) NULL,
# 	[attackString6] [varchar](255) NULL,
# 	[blockString1] [varchar](255) NULL,
# 	[blockString2] [varchar](255) NULL,
# 	[blockString3] [varchar](255) NULL,
# 	[merchantType] [varchar](50) NOT NULL,
# 	[merchantMarkup] [float] NOT NULL,
# 	[trainerType] [varchar](50) NOT NULL,
# 	[interactiveType] [varchar](50) NOT NULL,
# 	[gender] [int] NOT NULL,
# 	[race] [varchar](255) NOT NULL,
# 	[age] [int] NOT NULL,
# 	[patrol] [bit] NOT NULL,
# 	[patrolRoute] [varchar](1000) NULL,
# 	[immuneFire] [bit] NOT NULL,
# 	[immuneCold] [bit] NOT NULL,
# 	[immunePoison] [bit] NOT NULL,
# 	[immuneLightning] [bit] NOT NULL,
# 	[immuneCurse] [bit] NOT NULL,
# 	[immuneDeath] [bit] NOT NULL,
# 	[immuneStun] [bit] NOT NULL,
# 	[immuneFear] [bit] NOT NULL,
# 	[immuneBlind] [bit] NOT NULL,
# 	[spells] [varchar](max) NULL,
# 	[quests] [varchar](max) NULL,
# 	[groupAmount] [smallint] NOT NULL,
# 	[groupMembers] [varchar](1000) NULL,
# 	[weaponRequirement] [varchar](255) NULL,
# 	[questFlags] [varchar](255) NULL,

# rubocop:disable Layout/LineLength

# Vendor sample: Dr. Taylor
# INSERT [dbo].[NPC] ([catalogID], [npcID], [notes], [name], [attackSound], [deathSound], [idleSound], [movementString], [shortDesc], [longDesc], [visualKey], [classFullName], [baseArmorClass], [thac0Adjustment], [special], [npcType], [aiType], [species], [gold], [classType], [exp], [hitsMax], [alignment], [stamina], [manaMax], [speed], [strength], [dexterity], [intelligence], [wisdom], [constitution], [charisma], [lootVeryCommonAmount], [lootVeryCommonArray], [lootVeryCommonOdds], [lootCommonAmount], [lootCommonArray], [lootCommonOdds], [lootRareAmount], [lootRareArray], [lootRareOdds], [lootVeryRareAmount], [lootVeryRareArray], [lootVeryRareOdds], [lootLairAmount], [lootLairArray], [lootLairOdds], [lootAlwaysArray], [lootBeltAmount], [lootBeltArray], [lootBeltOdds], [spawnArmorArray], [spawnLeftHandArray], [spawnLeftHandOdds], [spawnRightHandArray], [mace], [bow], [dagger], [flail], [rapier], [twoHanded], [staff], [shuriken], [sword], [threestaff], [halberd], [thievery], [unarmed], [magic], [bash], [level], [animal], [tanningResult], [undead], [spectral], [hidden], [poisonous], [waterDweller], [fly], [breatheWater], [nightVision], [lair], [lairCells], [mobile], [command], [castMode], [randomName], [attackString1], [attackString2], [attackString3], [attackString4], [attackString5], [attackString6], [blockString1], [blockString2], [blockString3], [merchantType], [merchantMarkup], [trainerType], [interactiveType], [gender], [race], [age], [patrol], [patrolRoute], [immuneFire], [immuneCold], [immunePoison], [immuneLightning], [immuneCurse], [immuneDeath], [immuneStun], [immuneFear], [immuneBlind], [spells], [quests], [groupAmount], [groupMembers], [weaponRequirement], [questFlags]) VALUES (179, 20020, N'Sells Blindness Cure Potion', N'Dr.Taylor', NULL, NULL, NULL, N'', N'Fighter', NULL, N'male_merchant', N'Fighter', 10, 0, N'', N'Merchant', N'Unknown', N'Humanoid', 0, N'Fighter', 0, 0, N'Lawful', 0, 0, 3, 10, 9, 12, 10, 35, 35, 0, N'', 0, 0, N'', 0, 0, N'', 0, 0, N'', 0, 0, N'', 0, N'', 0, N'', 0, N'8900 15010', N'0', 0, N'', 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 0, 800, 0, 3000, 3, 0, N'', 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, 0, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'None', 1, N'None', N'Mugwort', 1, N'Illyria', 28800, 0, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 0, NULL, NULL, NULL)
# Vendor sample: Kesmai pawn shop
# INSERT [dbo].[NPC] ([catalogID], [npcID], [notes], [name], [attackSound], [deathSound], [idleSound], [movementString], [shortDesc], [longDesc], [visualKey], [classFullName], [baseArmorClass], [thac0Adjustment], [special], [npcType], [aiType], [species], [gold], [classType], [exp], [hitsMax], [alignment], [stamina], [manaMax], [speed], [strength], [dexterity], [intelligence], [wisdom], [constitution], [charisma], [lootVeryCommonAmount], [lootVeryCommonArray], [lootVeryCommonOdds], [lootCommonAmount], [lootCommonArray], [lootCommonOdds], [lootRareAmount], [lootRareArray], [lootRareOdds], [lootVeryRareAmount], [lootVeryRareArray], [lootVeryRareOdds], [lootLairAmount], [lootLairArray], [lootLairOdds], [lootAlwaysArray], [lootBeltAmount], [lootBeltArray], [lootBeltOdds], [spawnArmorArray], [spawnLeftHandArray], [spawnLeftHandOdds], [spawnRightHandArray], [mace], [bow], [dagger], [flail], [rapier], [twoHanded], [staff], [shuriken], [sword], [threestaff], [halberd], [thievery], [unarmed], [magic], [bash], [level], [animal], [tanningResult], [undead], [spectral], [hidden], [poisonous], [waterDweller], [fly], [breatheWater], [nightVision], [lair], [lairCells], [mobile], [command], [castMode], [randomName], [attackString1], [attackString2], [attackString3], [attackString4], [attackString5], [attackString6], [blockString1], [blockString2], [blockString3], [merchantType], [merchantMarkup], [trainerType], [interactiveType], [gender], [race], [age], [patrol], [patrolRoute], [immuneFire], [immuneCold], [immunePoison], [immuneLightning], [immuneCurse], [immuneDeath], [immuneStun], [immuneFear], [immuneBlind], [spells], [quests], [groupAmount], [groupMembers], [weaponRequirement], [questFlags]) VALUES (181, 20024, N'Kesmai Pawn Shop', N'Dik', NULL, NULL, NULL, N'', N'Fighter', NULL, N'male_merchant', N'Fighter', 10, 0, N'', N'Merchant', N'Unknown', N'Humanoid', 0, N'Fighter', 0, 0, N'Lawful', 0, 0, 3, 10, 9, 12, 13, 35, 35, 0, N'', 0, 0, N'', 0, 0, N'', 0, 0, N'', 0, 0, N'', 0, N'', 0, N'', 0, N'8010 15010', N'0', 0, N'', 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 1600, 0, 800, 0, 3000, 3, 0, N'', 0, 0, 0, 0, 0, 0, 0, 0, 0, N'', 0, 0, 0, 0, N'', N'', N'', N'', N'', N'', N'', N'', N'', N'Pawn', 3, N'None', N'None', 1, N'Mu', 28800, 0, NULL, 1, 1, 1, 1, 1, 1, 1, 1, 1, NULL, NULL, 0, NULL, NULL, NULL)
# INSERT [dbo].[SpawnZone] ([zoneID], [notes], [enabled], [npcID], [spawnTimer], [maxAllowedInZone], [spawnMessage], [minZone], [maxZone], [npcList], [spawnLand], [spawnMap], [spawnX], [spawnY], [spawnZ], [spawnRadius], [spawnZRange]) VALUES (51, N'Kesmai Pawn Shop', 1, 20024, 160, 1, NULL, 0, 0, NULL, 0, 0, 51, 31, 0, 0, NULL)

# CREATE TABLE [dbo].[SpawnZone](
#   [zoneID] [int] IDENTITY(1,1) NOT NULL,
#   [notes] [varchar](255) NULL,
#   [enabled] [bit] NOT NULL,
#   [npcID] [int] NOT NULL, /* from NPC table */
#   [spawnTimer] [int] NOT NULL, /* How many Janitor events (10 sec?) before respawn */
#   [maxAllowedInZone] [int] NOT NULL, /* Will respawn up to this amount (if minZone or maxZone are 0) */
#   [spawnMessage] [varchar](255) NULL,
#   [minZone] [int] NOT NULL, /* If both non-zero, number to respawn up to will be random between this... */
#   [maxZone] [int] NOT NULL, /* ... and this */
#   [npcList] [varchar](255) NULL, /* space-delimited list of npcIDs (from NPC table) */
#   [spawnLand] [int] NOT NULL, /* Spawn location... */
#   [spawnMap] [int] NOT NULL, /* ... */
#   [spawnX] [int] NOT NULL, /* ... */
#   [spawnY] [int] NOT NULL, /* ... */
#   [spawnZ] [int] NOT NULL, /* ... */
#   [spawnRadius] [int] NOT NULL, /* ...and 2d radius */
#   [spawnZRange] [varchar](255) NULL, /* space-delimited list of possible Z values */

# Spawn sample: Test Dog
# INSERT [dbo].[SpawnZone] ([zoneID], [notes],     [enabled], [npcID], [spawnTimer], [maxAllowedInZone], [spawnMessage], [minZone], [maxZone], [npcList], [spawnLand], [spawnMap], [spawnX], [spawnY], [spawnZ], [spawnRadius], [spawnZRange])
#    VALUES (               1,        N'Test Dog', 1,         920,     300,          1,                  NULL,           1,         1,         N'920',    0,           0,          44,       29,       0,        0,             NULL)

# rubocop:enable Layout/LineLength
