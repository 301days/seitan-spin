# frozen_string_literal: true

PROTOCOL_STRINGS = {
  SET_PROTOCOL: "\e98\e",
  VERSION_CLIENT: "\eV2\e",
  VERSION_CLIENT_END: "\eV3\e",
  NEWS: "\eM1\e",
  NEWS_END: "\eM2\e"
}.freeze

When(/^I send the protocol command "([^"]*)"$/) do |command|
  telnet_command(@connection, PROTOCOL_STRINGS[command.to_sym], /.../)
end

Then(/^I receive a CLIENT_VERSION of "([^"]*)"$/) do |version|
  expectation = PROTOCOL_STRINGS[:VERSION_CLIENT] +
                version +
                PROTOCOL_STRINGS[:VERSION_CLIENT_END]
  expect(@last_resp).to include(expectation)
end

Then(/^I receive a NEWS of "([^"]*)"$/) do |news|
  expectation = PROTOCOL_STRINGS[:NEWS] +
                news +
                PROTOCOL_STRINGS[:NEWS_END]
  expect(@last_resp).to include(expectation)
end

# #region Commands
# public static string PING = (char)27 + "88" + (char)27;
# public static string DELETE_CHARACTER = (char)27 + "89" + (char)27;
# public static string CHARGEN_RECEIVE = (char)27 + "90" + (char)27;
# public static string GET_SCORES = (char)27 + "91" + (char)27;
# public static string GOTO_GAME = (char)27 + "92" + (char)27;
# public static string GOTO_CHARGEN = (char)27 + "93" + (char)27;
# public static string GOTO_MENU = (char)27 + "94" + (char)27;
# public static string GOTO_CONFERENCE = (char)27 + "95" + (char)27;
# public static string LOGOUT = (char)27 + "96" + (char)27;
# public static string SWITCH_CHARACTER = (char)27 + "97" + (char)27;
# public static string SET_PROTOCOL = (char)27 + "98" + (char)27;
# public static string SET_CLIENT = (char)27 + "99" + (char)27;
# #endregion

# #region Version Information
# public static string VERSION_SERVER = (char)27 + "V0" + (char)27;
# public static string VERSION_SERVER_END = (char)27 + "V1" + (char)27;
# public static string VERSION_CLIENT = (char)27 + "V2" + (char)27;
# public static string VERSION_CLIENT_END = (char)27 + "V3" + (char)27;
# #endregion

# #region Main Menu, News, Detect Protocol/Client
# public static string MENU_MAIN = (char)27 + "M0" + (char)27;
# public static string NEWS = (char)27 + "M1" + (char)27;
# public static string NEWS_END = (char)27 + "M2" + (char)27;
# public static string DETECT_PROTOCOL = (char)27 + "M3" + (char)27;
# public static string DETECT_CLIENT = (char)27 + "M4" + (char)27;
# public static string MESSAGEBOX = (char)27 + "M5" + (char)27;
# public static string MESSAGEBOX_END = (char)27 + "M6" + (char)27;
# #endregion
