@reset_server
Feature: Login and account creation

Scenario: Logging in a new user
Given a new random user
And a telnet connection
When I create an account and character
Then I can play the game

Scenario: Logging in an existing user
Given a new random user
And a telnet connection
When I create an account and character
And I log out
And I get disconnected
And I log in
Then I can play the game

Scenario: Logging in an existing user - initially wrong password
Given a new random user
And a telnet connection
When I create an account and character
And I log out
And I log in with the wrong then right password
Then I can play the game

Scenario: Bad account name - silly
Given a new random user
And a silly account name
And a telnet connection
Then I try to create an account and the account name is rejected

Scenario: Bad account name - profane
Given a new random user
And a profane account name
And a telnet connection
Then I try to create an account and the account name is rejected

@slow
Scenario: Logging in a new user to check stats - Illyria
Given a new random user
And a telnet connection
When I create an account to verify that race "I" has the following stat constraints:
    | stat         | min  | max |
    | strength     | 3    | 18  |
    | dexterity    | 3    | 18  | 
    | intelligence | 3    | 17  | 
    | wisdom       | 4    | 18  | 
    | constitution | 4    | 18  | 
    | charisma     | 3    | 18  | 
#   | stamina      | 4    | 11  | # race not being considered in the server code
Then I can play the game

@slow
Scenario: Logging in a new user to check stats - Mu
Given a new random user
And a telnet connection
When I create an account to verify that race "M" has the following stat constraints:
    | stat         | min  | max |
    | strength     | 5    | 18  |
    | dexterity    | 3    | 18  | 
    | intelligence | 3    | 17  | 
    | wisdom       | 3    | 17  | 
    | constitution | 4    | 18  | 
    | charisma     | 3    | 18  | 
Then I can play the game

@slow
Scenario: Logging in a new user to check stats - Lemuria
Given a new random user
And a telnet connection
When I create an account to verify that race "L" has the following stat constraints:
    | stat         | min  | max |
    | strength     | 3    | 18  |
    | dexterity    | 4    | 18  | 
    | intelligence | 3    | 18  | 
    | wisdom       | 3    | 17  | 
    | constitution | 3    | 17  | 
    | charisma     | 4    | 18  | 
Then I can play the game

@slow
Scenario: Logging in a new user to check stats - Leng
Given a new random user
And a telnet connection
When I create an account to verify that race "LG" has the following stat constraints:
    | stat         | min  | max |
    | strength     | 3    | 18  |
    | dexterity    | 5    | 18  | 
    | intelligence | 3    | 17  | 
    | wisdom       | 3    | 18  | 
    | constitution | 4    | 18  | 
    | charisma     | 3    | 16  | 
#   | stamina      | 4    | 11  | # race not being considered in the server code
Then I can play the game

@slow
Scenario: Logging in a new user to check stats - Draznia
Given a new random user
And a telnet connection
When I create an account to verify that race "D" has the following stat constraints:
    | stat         | min  | max |
    | strength     | 3    | 18  |
    | dexterity    | 4    | 18  | 
    | intelligence | 5    | 18  | 
    | wisdom       | 3    | 18  | 
    | constitution | 3    | 16  | 
    | charisma     | 3    | 18  | 
Then I can play the game

@slow
Scenario: Logging in a new user to check stats - Hovath
Given a new random user
And a telnet connection
When I create an account to verify that race "H" has the following stat constraints:
    | stat         | min  | max |
    | strength     | 3    | 18  |
    | dexterity    | 3    | 18  | 
    | intelligence | 4    | 18  | 
    | wisdom       | 5    | 18  | 
    | constitution | 3    | 18  | 
    | charisma     | 3    | 17  | 
Then I can play the game

@slow
Scenario: Logging in a new user to check stats - Mnar
Given a new random user
And a telnet connection
When I create an account to verify that race "MN" has the following stat constraints:
    | stat         | min  | max |
    | strength     | 5    | 18  |
    | dexterity    | 3    | 18  | 
    | intelligence | 3    | 17  | 
    | wisdom       | 3    | 18  | 
    | constitution | 3    | 18  | 
    | charisma     | 4    | 18  | 
#   | stamina      | 5    | 12  | # race not being considered in the server code
Then I can play the game

@slow
Scenario: Logging in a new user to check stats - Barbarian
Given a new random user
And a telnet connection
When I create an account to verify that race "B" has the following stat constraints:
    | stat         | min  | max |
    | strength     | 5    | 18  |
    | dexterity    | 3    | 17  | 
    | intelligence | 3    | 16  | 
    | wisdom       | 3    | 16  | 
    | constitution | 5    | 18  | 
    | charisma     | 3    | 18  | 
Then I can play the game

# private static int GetRacialBonus(Character ch, string stat)
# {
#     switch (ch.race)
#     {
#         case "Illyria":
#             if (stat == "stamina" || stat == "wisdom" || stat == "constitution")
#                 return 1;
#             else if (stat == "intelligence")
#                 return -1;
#             return 0;
#         case "Mu":
#             if (stat == "strength")
#                 return 2;
#             else if (stat == "constitution")
#                 return 1;
#             else if (stat == "intelligence" || stat == "wisdom")
#                 return -1;
#             return 0;
#         case "Lemuria":
#             if (stat == "dexterity" || stat == "charisma")
#                 return 1;
#             else if (stat == "wisdom" || stat == "constitution")
#                 return -1;
#             return 0;
#         case "Leng":
#             if (stat == "dexterity")
#                 return 2;
#             else if (stat == "constitution" || stat == "stamina")
#                 return 1;
#             else if (stat == "intelligence")
#                 return -1;
#             else if (stat == "charisma")
#                 return -2;
#             return 0;
#         case "Draznia":
#             if (stat == "dexterity")
#                 return 1;
#             else if (stat == "intelligence")
#                 return 2;
#             else if (stat == "constitution")
#                 return -2;
#             return 0;
#         case "Hovath":
#             if (stat == "wisdom")
#                 return 2;
#             else if (stat == "intelligence")
#                 return 1;
#             else if (stat == "charisma")
#                 return -1;
#             return 0;
#         case "Mnar":
#             if (stat == "strength" || stat == "stamina")
#                 return 2;
#             else if (stat == "intelligence")
#                 return -1;
#             else if (stat == "charisma")
#                 return 1;
#             return 0;
#         case "Barbarian":
#             if (stat == "strength" || stat == "constitution")
#                 return 2;
#             else if (stat == "wisdom" || stat == "intelligence")
#                 return -2;
#             else if (stat == "dexterity")
#                 return -1;
#             return 0;
#         default:
#             return 0;
#     }
