@kill_server_after
Feature: Connection to server

Scenario: Disallow connection if server in Starting mode
Given I use the "production" database as-is
When the server executable is started
And I do not allow time for the server to complete startup
Then I can not connect to the server via telnet

Scenario: Allow connection if server in Running mode
Given I use the "minimal" database as-is
When the server executable is started
And I allow time for the server to complete startup
Then I can connect to the server via telnet

Scenario: Allow connection if server in Locked mode
Given I use the "minimal" database as-is
When the server executable is started
And I allow time for the server to complete startup
When I log on using a developer account
And I enter the chat
And I issue the lockserver command
And I exit the chat and am immediately disconnected
Then I log in

Scenario: Disallow connection if server in ShuttingDown mode
Given I use the "minimal" database as-is
When the server executable is started
And I allow time for the server to complete startup
When I log on using a developer account
And I enter the chat
And I issue the shutdown command and am immediately disconnected
Then I can not connect to the server via telnet
