# frozen_string_literal: true

require 'ansisys'
require 'nkf'

def test_dbg_mode
  ENV['TEST_DEBUG'].casecmp('true').zero?
end

def debug_msg(message)
  puts "#{message} # #{caller.first.partition('seitan-spin/').last}" if test_dbg_mode
end

def telnet_out(string)
  return if string.nil? || ENV['TELNET_TRACE'].nil? || ENV['TELNET_TRACE'].casecmp('false').zero?

  puts(ENV['TELNET_TRACE'].casecmp('html').zero? ? ansi_html(string) : ansi_strip(string))
end

def ansi_strip(string)
  @terminal ||= AnsiSys::Terminal.new
  @terminal.echo(string)
  @terminal.render(:text)
end

def ansi_html(string)
  @terminal ||= AnsiSys::Terminal.new
  @terminal.echo(string)
  "<tt>#{@terminal.render(:html, 80, nil, AnsiSys::Screen.default_css_colors(false, true))}</tt>"
end
